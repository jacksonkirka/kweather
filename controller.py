'''
Module controller.py is the control module for providing data to
KWeatherApp.
'''
import certifi
from datetime import datetime
from kivy.network.urlrequest import UrlRequest
from kivy.clock import Clock

class KWeatherController(object):
    '''
    KWeatherController is the weather controller for KWeather that
    controls weather data but not the Kivy interface.
    
    '''

    def __init__(self):
         self.weatherapi = \
         'https://api.openweathermap.org/data/2.5/weather?q=Cincinnati&appid=0f398ba1a72af994e50eefb225d1e398'
    def synchronous_api_request(self, url):
        '''
        Method synchronous_api_request is generic synchronoous request.
        '''
        req = UrlRequest(url, ca_file=certifi.where()) # Restful API Get Request
        req.wait(delay=.1) # synchronous request
        return req.result # return data to caller

    def asynchronous_api_request(self, url):
        '''
        Method asynchronous_api_request is coded using Kivy urlrequest.py.
        '''
        Clock.start_clock()         # Start the Kivy clock
        req = UrlRequest(url)       # get request thread
        while not req.is_finished:  # start while loop to test is_finished
            Clock.tick()            # tick clock per cycle
        Clock.stop_clock()          # Stop the clock
        return req.result           # Return the results

    def weather_api_update(self):
        '''
        Method weather_api_update returns json weather record and converts
        it to Python dictionary type returning all weather data to main.py.  
        '''
        weather_data = self.asynchronous_api_request(self.weatherapi)
        self.num = "{:.2f}".format((dict(weather_data)['main']['temp']-273.15)*9/5+32)
        temp = self.num + u'\N{DEGREE SIGN}' + ' F'
        update_color = self.change_temp_color(self.num)

        # Wind, visibility, pressure and humidity data
        windspeed = 'Wind: '+str(weather_data['wind']['speed'])+\
            ' / '+str(weather_data['wind']['deg'])+u'\N{DEGREE SIGN}'
        visibility = 'Visibility: '+str(weather_data['visibility'])
        pressure = 'Pressure: '+str(weather_data['main']['pressure'])
        humidity = 'Humidity: '+str(weather_data['main']['humidity'])
        # time stamp for strftime %H:%M:%S %p not printing on android
        stamp = datetime.now().strftime(
                '%A, %B %d, %Y  %H:%M:%S %p')
        return temp, windspeed, visibility, pressure, humidity, update_color, stamp

    def request_error(self):
        '''
        Method request_error handles any errors returned from the request.
        '''
        pass
    
    def change_temp_color(self, temp):
        '''
        Method temp color returns the color value for a float representing
        the temperature in Farenheit.  The algorithm return the product a float
        times the color value for change in temperature.
        '''
        
        tmp_flt = float(temp) # self.num must be float
        # Normal Gray
        if tmp_flt > 64.99 and tmp_flt < 74.99:
            color = (.33,.33,.33)
            return color # stop here and return color
        # Warm Red
        elif tmp_flt > 74.99:
            kv_color = (tmp_flt * 2.5)/255
            if kv_color > 1:
                kv_color = 1
            color = (kv_color, 0, 0) # Red
            return color # stop here and return color
        # If above tests fail no test needed here for Blue
        kv_color = (tmp_flt * 3.92)/255
        if kv_color > 1:
            kv_color = 1
        color = (0,0,kv_color) # Blue
        return color # stop here and return color


